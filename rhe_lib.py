#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Library for the classes related to the resolution of RHE

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from matplotlib.animation import FuncAnimation
from mpl_toolkits.mplot3d import Axes3D
from netCDF4 import Dataset
import warnings
import time
import os

gitlink = "https://gitlab.com/eneko.martin.martinez/" + \
           "project-for-scientific-computing-m2-efm"


class RHE_solve(object):
    """
    Class for a RHE solver.

    ...

    Attributes
    ----------
    n_it : int
        the current number of __iteration__s. Default 0.
    eps : float
        the constant to avoid machine rounding error when dividing. \
Default 1e-12.
    y : ndarray
        the current integration values.
    ys : ndarray
        the stacked integrations values.
    dt : float
        the time step of the integration.
    stack_freq : int
        the frequency of stacking values in ys.
    times : ndarray (1D)
        the vector of times of the stacked values.
    v : float (0<=1)
        the diffussion coefficient.
    jac_it : int, only for cr method
        the maximum number of __iteration__s of the Jacobi method for \
Crank-Nicolson integration method. Default 50.
    jac_prec : float, only for cr method
        the relative precision of the Jacobi method for \
Crank-Nicolson integration method. Default 1e-14.
    """
    n_it = 0
    eps = 1e-12

    # General initialization
    def __init__(self, y0, dt, v, stack_freq):
        self.y = y0.copy()
        self.ys = y0.copy()
        self.dt = dt
        self.stack_freq = stack_freq
        self.times = np.zeros(1)
        self.v = v
        # If cr integration add jac_it and jac_prec
        if (self.__class__).__name__[-2:] == 'cr':
            self.jac_it = 500
            self.jac_prec = 1e-8

    # Warning for jacobi in cr integration
    @staticmethod
    def __WarningMaxIt__(niter, prec):
        """
        Raises a warning if the maximum number of iterations has been \
reached during a Jacobi resolution process.

        Parameters
        ----------
        niter : int
            the number of iterations used.
        prec : float
            the goal precision to be reached.

        """
        warnings.warn("The precision of {} has not ".format(prec) +
                      "been reached in {} iterations...".format(niter),
                      RuntimeWarning)

    # Saving method
    def save(self, name, force=False):
        """
        Exports the data to a NetCDF file.

        Parameters
        ----------
        name : str
            the name of the output.
        force : bool, optional
            if exist a file with the same name and force is True, it will\
delete the existing file and create a new one. Defaul False.

        It adds '.nc' to the name if it does not end like that.
        """
        if name[-3:] != '.nc':
            name += '.nc'
        if os.path.isfile(name):
            if force:
                os.remove(name)
            else:
                raise FileExistsError("The file called " + name +
                                      " already exist in the directori" +
                                      " use force=True to rewrite.")
        # General attributes
        data = Dataset(name, "w", format="NETCDF4")
        data.description = "Integration of RHE equation"
        data.source = "Generated with the RHE integration tools " + gitlink
        data.history = "Created " + time.ctime(time.time())
        data.model = (self.__class__).__name__
        # Dimensions and variables declarations and attributes
        data.createDimension("time", len(self.times))
        v_t = data.createVariable("time", "d", ("time"))
        v_t.units = "s"
        v_t.description = "Time values."
        v_t[:] = self.times
        # Variables only in 1D
        if data.model[-5:-3] == '1D':
            data.createDimension("x", self.dim)
            v_x = data.createVariable("x", "d", ("x"))
            v_x[:] = self.x
            v_z = data.createVariable("heat", "d", ("time", "x"))
            v_z[:, :] = self.ys
        # Variables only in 2D
        if data.model[-5:-3] == '2D':
            data.createDimension("x", self.dim[1])
            v_x = data.createVariable("x", "d", ("x"))
            v_x[:] = self.grid[0][0, :]
            data.createDimension("y", self.dim[0])
            v_y = data.createVariable("y", "d", ("y"))
            v_y[:] = self.grid[1][:, 0]
            v_y.units = "m"
            v_y.description = "y axis."
            v_z = data.createVariable("heat", "d", ("time", "x", "y"))
            v_z[:, :, :] = np.transpose(self.ys)
        # Common variables 1D/2D
        v_x.units = "m"
        v_x.description = "x axis."
        v_z.units = "J"
        v_z.description = "Heat values."
        v_dt = data.createVariable("dt", "d")
        v_dt.units = "s"
        v_dt.description = "Time step."
        v_dt.assignValue(self.dt)
        v_v = data.createVariable("v", "d")
        v_v.description = "Speed speed light units"
        v_v.assignValue(self.v)
        v_sf = data.createVariable("sf", "i")
        v_sf.description = "Stacking frequency"
        v_sf.assignValue(self.stack_freq)
        v_ep = data.createVariable("eps", "d")
        v_ep.units = "J^2"
        v_ep.description = "Epsilon to avoid numerical error"
        v_ep.assignValue(self.eps)
        # Variables only for Crank-Nicolson
        if data.model[-2:] == 'cr':
            v_ji = data.createVariable("ji", "i")
            v_ji.description = "Maximum number of iterations for Jacobi"
            v_ji.assignValue(self.jac_it)
            v_jp = data.createVariable("jp", "d")
            v_jp.description = "Precision for Jacobi"
            v_jp.assignValue(self.jac_prec)
        data.close()


class RHE_solve_1D(RHE_solve):
    """
    Class for a RHE 1D solver.

    ...

    Attributes
    ----------
    x : ndarray (1D)
        the vector of integration nodes.
    dim : int
        the lenght of x.
    h : double
        thestep in the nodes vector. Must be constant.
    k : float (>0)
        the 0.5*v*dt/h**2 constant.

    see help(__RHS___solve) for extra documentation
    """
    # Specific initialization of 1D
    def __init__(self, x, y0, dt, v, stack_freq):
        super().__init__(y0, dt, v, stack_freq)
        self.x = x.copy()
        self.dim = len(x)
        if len(self.y) != self.dim:
            raise ValueError("x and y0 should have the same length")
        dif = np.diff(self.x)
        self.h = dif[0]
        if np.any(dif-self.h > 1e-7):
            raise ValueError("x is not equispaced")
        self.k = 0.5*self.v*self.dt/self.h**2

    def __cA__(self, u):
        """
        Calculation of k*A(u_j^n, u_{j + 1}^n) values for the integration.

        Parameters
        ----------
        u : ndarray (1D)
            the vector of the values used to calculate A.

        Returns
        -------
        result : ndarray (1D)
            the vector of k*a_{j + 1/2}^n values
        """
        usum = u[:-1] + u[1:]
        result = self.k*usum/np.sqrt(.25*usum**2 +
                                     (self.v*np.diff(u)/self.h)**2 +
                                     self.eps)
        return result

    def __RHS__(self, u, a):
        """
        Returns the right hand side of the explicit or Crank-Nicolson method.

        Parameters
        ----------
        u : ndarray (1D)
            the vector of the values used to calculate the rhs.
        a: ndarray (1D)
            the vector of a_{j + 1/2} values.

        Returns
        -------
        rhs : ndarray (1D)
            the vector of rhs values
        """
        rhs = (1-a[:-1]-a[1:])*u[1:-1] + a[:-1]*u[:-2] + a[1:]*u[2:]
        return rhs

    def animation(self, show=True, interval=200, repeat=False, **kwards):
        """
        Shows an animation of the evolution of the RHE integration\
using the stacked values in ys.

        Parameters
        ----------
        show : bool, optional
            if True shows the plot. If False returns animation, which can be \
used to save the animation data or show it in interfaces as jupyter notebook. \
Default True.
        interval : number, optional
            Delay between frames in milliseconds. Defaults to 200.
        repeat : bool, optional
            Controls whether the animation should repeat when the sequence \
of frames is completed. Defaults to True.
        **kwards : '.Line2D' properties, optional
            *kwargs* are used to specify properties like a line label \
(for auto legends), linewidth, antialiasing, marker face color. \
See matplotlib.pyplot.plot documentation.
        """
        yrang = (0, 1.1*np.max(self.ys))
        xrang = (np.min(self.x) - 0.5*self.h, np.max(self.x) + 0.5*self.h)
        plt.ioff()
        fig = plt.figure(figsize=[6, 5])
        frames = len(self.times)

        def animate(i):
            plt.clf()
            plt.plot(self.x, self.ys[i, :], **kwards)
            plt.title("t={:.3f}".format(self.times[i]))
            plt.xlim(xrang)
            plt.ylim(yrang)
            plt.grid()
        # Generate animation object
        ani = FuncAnimation(fig, animate, frames=frames,
                            interval=interval, repeat=repeat)
        if show:
            plt.show()
        else:
            return ani

    def display(self, value=None, show=True, **kwards):
        """
        Makes a plot of the RHE integration.

        Parameters
        ----------
        value : int or None, optional
            the index of the stacked value to be plotted. If None, \
plot the current value y. Default None.
        show : bool, optional
            if True shows the plot. False can be used to make a \
simultaneous plot of different values or objects. Default True.
        **kwards : '.Line2D' properties, optional
            *kwargs* are used to specify properties like a line label \
(for auto legends), linewidth, antialiasing, marker face color. \
See matplotlib.pyplot.plot documentation.
        """
        if value is None:
            sy = self.y
            time = self.dt*self.n_it
        else:
            sy = self.ys[value, :]
            time = self.times[value]
        plt.plot(self.x, sy, **kwards)
        plt.title("t={:.3f}".format(time))
        plt.xlim(np.min(self.x) - 0.5*self.h, np.max(self.x) + 0.5*self.h)
        plt.grid()
        if show:
            plt.show()
            plt.clf()


class RHE_solve_2D(RHE_solve):
    """
    Class for a RHE 2D solver.

    ...

    Attributes
    ----------
    grid : ndarray (2D)
        the grid of integration nodes.
    dim : int
        the shape of the integration grid.
    h : double
        thestep in the nodes vector. Must be constant.
    k : float (>0)
        the 0.5*v*dt/h**2 constant.

    see help(__RHS___solve) for extra documentation
    """
    # Specific initialization of 2D
    def __init__(self, grid, y0, dt, v, stack_freq):
        super().__init__(y0, dt, v, stack_freq)
        self.grid = grid.copy()
        self.dim = grid[0].shape
        if (self.y).shape != self.dim:
            raise ValueError("x and y0 should have the same length")
        dif0 = np.diff(self.grid[0], axis=1)
        dif1 = np.diff(self.grid[1], axis=0)
        self.h = dif0[0, 0]
        if np.any(dif0-self.h > 1e-7) | np.any(dif1-self.h > 1e-7):
            raise ValueError("x is not equispaced")
        self.k = .5*self.v*self.dt/self.h**2

    def __cA__(self, u, axis):
        """
        Calculation of k*A(u_{j, k}^n, u_{j + 1, k}^n) or \
k*b(u_{j, k}^n, u_{j, k + 1}^n) values for the integration.

        Parameters
        ----------
        u : ndarray (2D)
            the vector of the values used to calculate A or B.
        axis: int (0 or 1)
            if axis is 0 A values will be computed if axis is 1 B\
values will be computed.

        Returns
        -------
        result : ndarray (2D)
            the vector of k*a_{j + 1/2, k}^n or k*b_{j, k + 1/2}^n values.
        """
        if axis == 0:
            mid = .25 * (u[:-1, 2:]+u[1:, 2:]-u[:-1, :-2]-u[1:, :-2])
            usum = u[:-1, 1:-1] + u[1:, 1:-1]
            udif = np.diff(u[:, 1:-1], axis=0)
        elif axis == 1:
            mid = .25 * (u[2:, :-1]+u[2:, 1:]-u[:-2, :-1]-u[:-2, 1:])
            usum = u[1:-1, :-1] + u[1:-1, 1:]
            udif = np.diff(u[1:-1, :], axis=1)
        result = self.k*usum/np.sqrt(.25*usum**2 +
                                     (self.v/self.h)**2*(udif**2+mid**2) +
                                     self.eps)
        return result

    def __RHS__(self, u, a, b):
        """
        Returns the right hand side of the explicit or \
Crank-Nicolson method

        Parameters
        ----------
        u : ndarray (2D)
            the array of the values used to calculate the rhs.
        a: ndarray (2D)
            the array of a_{j + 1/2, k} values.
        b: ndarray (2D)
            the array of b_{j, k + 1/2} values.

        Returns
        -------
        rhs : ndarray (2D)
            the array of rhs values
        """
        rhs = a[:-1, :]*u[:-2, 1:-1] + b[:, :-1]*u[1:-1, :-2] + \
            a[1:, :]*u[2:, 1:-1] + b[:, 1:]*u[1:-1, 2:] + \
            u[1:-1, 1:-1]*(1-a[:-1, :]-b[:, :-1]-a[1:, :]-b[:, 1:])
        return rhs

    def animation_cs(self, axis=0, cut=None, show=True, interval=200,
                     repeat=False, **kwards):
        """
        Shows an animation the evolution of the cross-section \
of the RHE integration using the stacked values in ys.

        Parameters
        ----------
        axis: int (0 or 1), optional
            the axis included in the cross section. Default 0.
        cut: int or None, optional
            the index where the other axis will be cutted. If None, \
the other axis will be cutted in the middle. Default None.
        show : bool, optional
            if True shows the plot. If False returns animation, which can be \
used to save the animation data or show it in interfaces as jupyter notebook. \
Default True.
        interval : number, optional
            Delay between frames in milliseconds. Defaults to 200.
        repeat : bool, optional
            Controls whether the animation should repeat when the sequence \
of frames is completed. Defaults to True.
        **kwards : '.Line2D' properties, optional
            *kwargs* are used to specify properties like a line label \
(for auto legends), linewidth, antialiasing, marker face color. \
See matplotlib.pyplot.plot documentation.
        """
        if cut is None:
            cut = int(0.5*self.dim[axis])
        x2 = np.take(self.grid[axis], cut, axis=axis)
        y2 = np.take(self.ys, cut, axis=axis)
        yrang = (0, 1.1*np.max(y2))
        xrang = (np.min(x2) - 0.5*self.h, np.max(x2) + 0.5*self.h)
        plt.ioff()
        fig = plt.figure(figsize=[6, 5])
        frames = len(self.times)

        def animate(i):
            plt.clf()
            plt.plot(x2, y2[:, i], **kwards)
            plt.title("t={:.3f}".format(self.times[i]))
            plt.xlim(xrang)
            plt.ylim(yrang)
            plt.grid()
        # Generate animation object
        ani = FuncAnimation(fig, animate, frames=frames,
                            interval=interval, repeat=repeat)
        if show:
            plt.show()
        else:
            return ani

    def display_cs(self, value=None, show=True, axis=0, cut=None, **kwards):
        """
        Makes a plot of the cross-section of the RHE integration.

        Parameters
        ----------
        value : int or None, optional
            the index of the stacked value to be plotted. If None, \
plot the current value y. Default None.
        show : bool, optional
            if True shows the plot. False can be used to make \
a simultaneous plot of different values or objects. Default True.
        axis: int (0 or 1), optional
            the axis included in the cross section. Default 0.
        cut: int or None, optional
            the index where the other axis will be cutted. If None, \
the other axis will be cutted in the middle. Default None.
        **kwards : '.Line2D' properties, optional
            *kwargs* are used to specify properties like a line label \
(for auto legends), linewidth, antialiasing, marker face color. \
See matplotlib.pyplot.plot documentation.
        """
        if value is None:
            sy = self.y
            time = self.dt*self.n_it
        else:
            sy = self.ys[:, :, value]
            time = self.times[value]
        if cut is None:
            cut = int(0.5*self.dim[axis])
        x2 = np.take(self.grid[axis], cut, axis=axis)
        y2 = np.take(sy, cut, axis=axis)
        xrang = (np.min(x2) - 0.5*self.h, np.max(x2) + 0.5*self.h)
        plt.plot(x2, y2, **kwards)
        plt.title("t={:.3f}".format(time))
        plt.xlim(xrang)
        plt.grid()
        if show:
            plt.show()
            plt.clf()

    def animation_3d(self, show=True, interval=200, repeat=False):
        """
        Shows a 3D animation the evolution of the RHE integration \
using the stacked values in ys.

        Parameters
        ----------
        show : bool, optional
            if True shows the plot. If False returns animation, which can be \
used to save the animation data or show it in interfaces as jupyter notebook. \
Default True.
        interval : number, optional
            Delay between frames in milliseconds. Defaults to 200.
        repeat : bool, optional
            Controls whether the animation should repeat when the sequence \
of frames is completed. Defaults to True.
        """
        yrang = (0, 1.1*np.max(self.ys))
        x1rang = (np.min(self.grid[0]) - 0.5*self.h,
                  np.max(self.grid[0]) + 0.5*self.h)
        x2rang = (np.min(self.grid[1]) - 0.5*self.h,
                  np.max(self.grid[1]) + 0.5*self.h)
        plt.ioff()
        fig = plt.figure(figsize=[6, 5])
        frames = len(self.times)

        def animate(i):
            plt.clf()
            ax = fig.gca(projection='3d')
            ax.plot_surface(self.grid[0], self.grid[1], self.ys[:, :, i],
                            cmap=cm.jet, linewidth=0, antialiased=False)
            ax.set_xlim(x1rang)
            ax.set_ylim(x2rang)
            ax.set_zlim(yrang)
            ax.xaxis.set_major_locator(LinearLocator(5))
            ax.yaxis.set_major_locator(LinearLocator(5))
            ax.xaxis.set_major_formatter(FormatStrFormatter('%.01f'))
            ax.yaxis.set_major_formatter(FormatStrFormatter('%.01f'))
            plt.title("t={:.3f}".format(self.times[i]))
        # Generate animation object
        ani = FuncAnimation(fig, animate, frames=frames,
                            interval=interval, repeat=repeat)
        if show:
            plt.show()
        else:
            return ani

    def display_3d(self, value=None, show=True):
        """
        Makes a 3D plot of the RHE integration.

        Parameters
        ----------
        value : int or None, optional
            the index of the stacked value to be plotted. \
If None, plot the current value y. Default None.
        show : bool, optional
            if True shows the plot. False can be used to make a \
simultaneous plot of different values or objects. Default True.
        """
        if value is None:
            sy = self.y
            time = self.dt*self.n_it
        else:
            sy = self.ys[:, :, value]
            time = self.times[value]
        x1rang = (np.min(self.grid[0]) - 0.5*self.h,
                  np.max(self.grid[0]) + 0.5*self.h)
        x2rang = (np.min(self.grid[1]) - 0.5*self.h,
                  np.max(self.grid[1]) + 0.5*self.h)
        fig = plt.figure(figsize=[6, 5])
        ax = fig.gca(projection='3d')
        ax.plot_surface(self.grid[0], self.grid[1], sy,
                        cmap=cm.jet, linewidth=0, antialiased=False)
        ax.set_xlim(x1rang)
        ax.set_ylim(x2rang)
        ax.xaxis.set_major_locator(LinearLocator(5))
        ax.yaxis.set_major_locator(LinearLocator(5))
        ax.xaxis.set_major_formatter(FormatStrFormatter('%.01f'))
        ax.yaxis.set_major_formatter(FormatStrFormatter('%.01f'))
        plt.title("t={:.3f}".format(time))
        if show:
            plt.show()
            plt.clf()


class RHE_solve_1D_ex(RHE_solve_1D):
    """
    Class for a RHE 1D solver with explicit method.

    ...

    see help(__RHS___solve_1D) for extra documentation
    """
    def __init__(self, x, y0, dt, v=1., stack_freq=100):
        super().__init__(x, y0, dt, v, stack_freq)

    def __iteration__(self, u):
        """
        Performs an iteration of the explicit method.

        Parameters
        ----------
        u : ndarray (1D)
            the current vector of values.

        Returns
        ----------
        u : ndarray (1D)
            the new vector of values.
        """
        a = super().__cA__(u)
        u[1:-1] = super().__RHS__(u, a)
        u[0], u[-1] = u[1], u[-2]
        return u

    def forward(self, n=1):
        """
        Makes n steps forward on the integration.
        Adds the number of steps to n_it.
        Add the last step to y.
        Each time that n_it reachs stack_freq, it stacks the current\
value of y to ys and the current time to times.

        Parameters
        ----------
        n : int, optional
            the number of steps forward. Default 1.
        """
        for i in range(n):
            self.y = self.__iteration__(self.y)
            self.n_it += 1
            if (self.n_it % self.stack_freq == 0):
                self.ys = np.vstack((self.ys, self.y))
                self.times = np.hstack((self.times, self.n_it*self.dt))


class RHE_solve_1D_cr(RHE_solve_1D):
    """
    Class for a RHE 1D solver with Cranck-Nicolson method.

    ...

    see help(__RHS___solve_1D) for extra documentation
    """
    def __init__(self, x, y0, dt, v=1., stack_freq=100):
        super().__init__(x, y0, dt, v, stack_freq)

    def __iteration__(self, u):
        """
        Performs an iteration of the Crank-Nicolson method.
        It uses a loop with a Jacobi method for the resolution \
of the system, with a precission of jac_prec and a maximum of \
jac_it iterations. If the maximum is reached it will raise a warning.

        Parameters
        ----------
        u : ndarray (1D)
            the current vector of values.

        Returns
        ----------
        u : ndarray (1D)
            the new vector of values.
        """
        a = 0.5*(super().__cA__(u))
        rhs = super().__RHS__(u, a)
        for j in range(self.jac_it):
            un = (rhs + a[:-1]*u[:-2] + a[1:]*u[2:])/(1 + a[:-1] + a[1:])
            u[0], u[-1] = un[0], un[-1]
            if np.max(np.abs(u[1:-1]-un)) < self.jac_prec:
                u[1:-1] = un
                break
            u[1:-1] = un
            # Discoment to implicit C-N:
            #a = 0.5*(super().__cA__(u))
        if j == (self.jac_it-1):
            RHE_solve.__WarningMaxIt__(self.jac_it, self.jac_prec)
        return u

    def forward(self, n=1):
        """
        Makes n steps forward on the integration.
        Adds the number of steps to n_it.
        Add the last step to y.
        Each time that n_it reachs stack_freq, it stacks the current \
value of y to ys and the current time to times.

        Parameters
        ----------
        n : int, optional
            the number of steps forward. Default 1.
        """
        for i in range(n):
            self.y = self.__iteration__(self.y)
            self.n_it += 1
            if (self.n_it % self.stack_freq == 0):
                self.ys = np.vstack((self.ys, self.y))
                self.times = np.hstack((self.times, self.n_it*self.dt))


class RHE_solve_2D_ex(RHE_solve_2D):
    """
    Class for a RHE 2D solver with explicit method.

    ...

    see help(__RHS___solve_2D) for extra documentation
    """
    def __init__(self, grid, y0, dt, v=1., stack_freq=100):
        super().__init__(grid, y0, dt, v, stack_freq)

    def __iteration__(self, u):
        """
        Performs an iteration of the explicit method.

        Parameters
        ----------
        u : ndarray (2D)
            the current vector of values.

        Returns
        ----------
        u : ndarray (2D)
            the new vector of values.
        """
        a = super().__cA__(u, axis=0)
        b = super().__cA__(u, axis=1)
        u[1:-1, 1:-1] = super().__RHS__(u, a, b)
        u[0, :], u[-1, :] = u[1, :], u[-2, :]
        u[:, 0], u[:, -1] = u[:, 1], u[:, -2]
        return u

    def forward(self, n=1):
        """
        Makes n steps forward on the integration.
        Adds the number of steps to n_it.
        Add the last step to y.
        Each time that n_it reachs stack_freq, it stacks the current \
value of y to ys and the current time to times.

        Parameters
        ----------
        n : int, optional
            the number of steps forward. Default 1.
        """
        for i in range(n):
            self.y = self.__iteration__(self.y)
            self.n_it += 1
            if (self.n_it % self.stack_freq == 0):
                self.ys = np.dstack((self.ys, self.y))
                self.times = np.hstack((self.times, self.n_it*self.dt))


class RHE_solve_2D_cr(RHE_solve_2D):
    """
    Class for a RHE 2D solver with Cranck-Nicolson method.

    ...

    see help(__RHS___solve_2D) for extra documentation
    """
    def __init__(self, grid, y0, dt, v=1., stack_freq=100):
        super().__init__(grid, y0, dt, v, stack_freq)

    def __iteration__(self, u):
        """
        Performs an iteration of the Crank-Nicolson method.
        It uses a loop with a Jacobi method for the resolution \
of the system, with a precission of jac_prec and a maximum of \
jac_it iterations. If the maximum is reached it will raise a warning.

        Parameters
        ----------
        u : ndarray (2D)
            the current vector of values.

        Returns
        ----------
        u : ndarray (2D)
            the new vector of values.
        """
        a = 0.5*(super().__cA__(u, axis=0))
        b = 0.5*(super().__cA__(u, axis=1))
        rhs = super().__RHS__(u, a, b)
        for j in range(self.jac_it):
            div = 1 + a[:-1, :] + b[:, :-1] + a[1:, :] + b[:, 1:]
            un = (rhs +
                  a[:-1, :]*u[:-2, 1:-1] + b[:, :-1]*u[1:-1, :-2] +
                  a[1:, :]*u[2:, 1:-1] + b[:, 1:]*u[1:-1, 2:])/div
            u[0, 1:-1], u[-1, 1:-1] = un[0, :], un[-1, :]
            u[1:-1, 0], u[1:-1, -1] = un[:, 0], un[:, -1]
            if np.max(np.abs((u[1:-1, 1:-1]-un)/np.max(un))) < self.jac_prec:
                u[1:-1, 1:-1] = un
                break
            u[1:-1, 1:-1] = un
            # Discoment to implicit C-N:
            #a = 0.5*(super().__cA__(u, axis=0))
            #b = 0.5*(super().__cA__(u, axis=1))
        if j == (self.jac_it-1):
            RHE_solve.__WarningMaxIt__(self.jac_it, self.jac_prec)
        return u

    def forward(self, n=1):
        """
        Makes n steps forward on the integration.
        Adds the number of steps to n_it.
        Add the last step to y.
        Each time that n_it reachs stack_freq, it stacks the current \
value of y to ys and the current time to times.

        Parameters
        ----------
        n : int, optional
            the number of steps forward. Default 1.
        """
        for i in range(n):
            self.y = self.__iteration__(self.y)
            self.n_it += 1
            if (self.n_it % self.stack_freq == 0):
                self.ys = np.dstack((self.ys, self.y))
                self.times = np.hstack((self.times, self.n_it*self.dt))


def load(name):
    """
    Imports the data from a NetCDF file.

    Parameters
    ----------
    name : str
        the name of the input file.

    It adds '.nc' to the name if it does not end like that.

    Returns
    -------
    obj : object
        the object of the corresponding integration class.
    """
    if name[-3:] != '.nc':
        name += '.nc'
    if os.path.isfile(name):
        FileNotFoundError("The file called " + name +
                          " does not exist in the directori.")
    data = Dataset(name, "r", format="NETCDF4")
    model = data.model
    # Common variables 1D/2D needed for initializing object
    x = data.variables['x'][:]
    dt = data.variables['dt'].getValue()
    v = data.variables['v'].getValue()
    sf = data.variables['sf'].getValue()
    # Variables only in 1D needed for initializing object
    if model[-5:-3] == '1D':
        z = data.variables['heat'][:, :]
    # Variables only in 2D needed for initializing object
    if model[-5:-3] == '2D':
        y = data.variables['y'][:]
        grid = np.meshgrid(x, y)
        z = np.transpose(data.variables['heat'][:, :, :])
    # Create the object
    if model == 'RHE_solve_1D_ex':
        obj = RHE_solve_1D_ex(x, z[-1, :], dt, v=v, stack_freq=sf)
    elif model == 'RHE_solve_1D_cr':
        obj = RHE_solve_1D_cr(x, z[-1, :], dt, v=v, stack_freq=sf)
    elif model == 'RHE_solve_2D_ex':
        obj = RHE_solve_1D_ex(grid, z[:, :, -1], dt, v=v, stack_freq=sf)
    elif model == 'RHE_solve_2D_cr':
        obj = RHE_solve_1D_cr(grid, z[:, :, -1], dt, v=v, stack_freq=sf)
    else:
        ValueError("Invalid model name in the file")
    # Variables only for Crank-Nicolson
    if data.model[-2:] == 'cr':
        obj.jac_it = data.variables['ji'].getValue()
        obj.jac_prec = data.variables['jp'].getValue()
    # Running attributes
    obj.eps = data.variables['eps'].getValue()
    obj.ys = z
    obj.times = data.variables['time'][:]
    data.close()
    n_it = sf * (len(obj.times)-1)
    obj.n_it = n_it
    return obj
