﻿# Project for Scientific Computing M2 EFM

Development of a set of solvers for the relativistic heat diffusion equation (RHE) based on A. Marquina "Diffusion front capturing schemes for a class of Fokker–Planck equations: Application to the relativistic heat equation".

## Goals of the project

- [x] Developing a set of solvers for RHE using OOP.
- [x] Implementing saving and loading options in NetCDF format to make the program able to run previously started solutions.
- [x] Creating plotting functions/methods.
- [x] Developing a documented library for the previous goals.
- [x] Using a jupyter notebook to play with the functions and show the results.
- [x] Comparing and discussing the results with A. Marquina 2010.
- [x] Using git in the project development process.
- [x] Getting used to PEP8 style.

## Why these goals?
Last year a had a small tutorial of two hours in OOP, but I have never done OOP. I wanted to learn properly on my own how to work with classes in order to make my scripts more ordered and be able to save different type of data related in an object. 
In my previous works I have also had a lack of documentation, so was difficult to recover an old script or share them with other people without spending hours understanding them. For the same reason, I want to get used to the PEP8 style.
I have never used Jupyter at all and working with it is a way to make easier the visualization of the programming results in each step. 
I wanted also to learn better how to make animations, which can be useful in order to plot the evolution of a time dependent variable.
I have loaded data from NetCDF files before using [StarR library](https://cran.r-project.org/web/packages/startR/startR.pdf) in R, but I want to have a further knowledge in Python. Nevertheless, for the project I taken is not necessary to save the data, as the simulations do not take to much time and we do not want the data for other purpose, it is an opportunity to learn better how to create a properly netCDF file and how to load it with Python.
Finally, although it is not a Python skill, I wanted to make this project using Git, so I learn a way to be able to access to previous versions of code and cooperate with other people in the same project.


## Prerequisites

Python3, Jupyter Notebook and the following Python libraries: numpy, matplotlib, netCDF4, IPython, warnings, time, os 

## Running

The script _rhe\_lib.py_ provides all 4 different main classes to compute the integration of the RHE:

- _RHE\_solve\_1D\_ex_: integration of 1D RHE with explicit method
- _RHE\_solve\_1D\_cr_: integration of 1D RHE with Crank-Nicolson method
- _RHE\_solve\_2D\_ex_: integration of 2D RHE with explicit method
- _RHE\_solve\_2D\_cr_: integration of 2D RHE with Crank-Nicolson method

With the following methods:
- _forward_: computes the time integration
- _display_: makes a plot of the integration (only 1D)
- _display\_cs_: makes a plot of a cross-section of the integration (only 2D)
- _display\_3d_: makes a 3D plot of the integration (only 2D)
- _animation_: makes an animation of the evolution (only 1D)
- _animation\_cs_: makes an animation of a cross-section of the evolution (only 2D)
- _animation\_3d_: makes a 3D animation of the evolution (only 2D)
- _save_: exports the data to a NetCDF file

The function _load_ load previously saved data.

All the classes, methods and the function have been documented. So It is possible to access to more information using help(Class), help(method) or help(load).

The notebook _rhe.ipynb_ reproduces the most of the work of Marquina 2010, using the classes written in _rhe\_lib.py_. It is recommendable to run it to see the animations.


## Author

* **Eneko Martin Martinez** - [LinkedIn](https://www.linkedin.com/in/eneko-martin/)

[Link to the original project on GitLab](https://gitlab.com/eneko.martin.martinez/project-for-scientific-computing-m2-efm)


## References and acknowledgements

* A. Marquina. Diffusion front capturing schemes for a class of fokker–planck equations: Application to the relativistic heat equation. Journal of Computational Physics, 229:2659–2674, 04 2010. doi: [10.1016/j.jcp.2009.12.014](https://www.sciencedirect.com/science/article/pii/S0021999109006858?via%3Dihub)

* [Git Tutorial from David Mahler](https://www.youtube.com/user/mahler711/featured)
* Slides in OOP, [intro-python course Pierre Augier et al.](https://gricad-gitlab.univ-grenoble-alpes.fr/augierpi/intro-python)
* [netCDF4 module documentation](https://unidata.github.io/netcdf4-python/netCDF4/index.html)
* [PEP 8 -- Style Guide for Python Code](https://www.python.org/dev/peps/pep-0008/)
* Many Stack Overflow issues...
